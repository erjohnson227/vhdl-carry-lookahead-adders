library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 

entity lookahead_adder_16bit_tb is
end lookahead_adder_16bit_tb; 

architecture behav of lookahead_adder_16bit_tb is 

component lookahead_adder_16bit is
port (
    i16_a : in std_logic_vector(15 downto 0); 
    i16_b : in std_logic_vector(15 downto 0); 
    i_cin : in std_logic; 
    o16_sum : out std_logic_vector(15 downto 0); 
    o_cout : out std_logic;
    o_p : out std_logic; 
    o_g : out std_logic
);
end component; 

for lookahead_adder_16bit_0 : lookahead_adder_16bit use entity 
                              work.lookahead_adder_16bit; 

-- Signal definitions
signal a, b : std_logic_vector(15 downto 0); 
signal cin : std_logic; 
signal sum : std_logic_vector(15 downto 0); 
signal cout : std_logic;
signal p, g : std_logic; 

signal expected_sum : std_logic_vector(15 downto 0); 
signal expected_cout, expected_p, expected_g : std_logic; 

begin
    lookahead_adder_16bit_0 : lookahead_adder_16bit port map(
        i16_a => a, i16_b => b, i_cin => cin, o16_sum => sum, o_cout => cout, 
        o_p => p, o_g => g
    ); 

    process 
    variable case_no : integer; 
    begin
        


        -- Test case 0 ########################################################
        case_no := 0; 

        -- Set input values
        a <= std_logic_vector(to_unsigned(16#00#,a'length)); 
        b <= std_logic_vector(to_unsigned(16#00#,a'length)); 
        cin <= '0'; 

        -- Set expected output values
        expected_sum <= std_logic_vector(to_unsigned(16#00#,a'length)); 
        expected_cout <= '0'; 
        expected_p <= '0'; 
        expected_g <= '0'; 

        wait for 20 ns; 

        -- Assert on sum 
        assert sum = expected_sum
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Sum error." & LF & "Expected value = " & 
            integer'image(to_integer(unsigned(expected_sum))) & LF &
            "got value = " & integer'image(to_integer(unsigned(sum)))
            severity error;

        -- Assert on carry out
        assert cout = expected_cout
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Carry out error." & LF & "Expected value = " & 
            std_logic'image(expected_cout) & LF &
            "got value = " & std_logic'image(cout)
            severity error;

        -- Assert on p out
        assert p = expected_p
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": P out error." & LF & "Expected value = " & 
            std_logic'image(expected_p) & LF &
            "got value = " & std_logic'image(p)
            severity error;

        -- Assert on g out
        assert g = expected_g
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": G out error." & LF & "Expected value = " & 
            std_logic'image(expected_g) & LF &
            "got value = " & std_logic'image(g)
            severity error;


        case_no := case_no + 1; 

        -- Test case 1 ########################################################

        -- Set input values
        a <= std_logic_vector(to_unsigned(16#01#,a'length)); 
        b <= std_logic_vector(to_unsigned(16#03#,a'length)); 
        cin <= '0'; 

        -- Set expected output values
        expected_sum <= std_logic_vector(to_unsigned(16#04#,a'length)); 
        expected_cout <= '0'; 
        expected_p <= '0'; 
        expected_g <= '0'; 

        wait for 20 ns; 

        -- Assert on sum 
        assert sum = expected_sum
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Sum error." & LF & "Expected value = " & 
            integer'image(to_integer(unsigned(expected_sum))) & LF &
            "got value = " & integer'image(to_integer(unsigned(sum)))
            severity error;

        -- Assert on carry out
        assert cout = expected_cout
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Carry out error." & LF & "Expected value = " & 
            std_logic'image(expected_cout) & LF &
            "got value = " & std_logic'image(cout)
            severity error;

        -- Assert on p out
        assert p = expected_p
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": P out error." & LF & "Expected value = " & 
            std_logic'image(expected_p) & LF &
            "got value = " & std_logic'image(p)
            severity error;

        -- Assert on g out
        assert g = expected_g
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": G out error." & LF & "Expected value = " & 
            std_logic'image(expected_g) & LF &
            "got value = " & std_logic'image(g)
            severity error;


        case_no := case_no + 1; 

        -- Test case 2 ########################################################

        -- Set input values
        a <= std_logic_vector(to_unsigned(16#08#,a'length)); 
        b <= std_logic_vector(to_unsigned(16#03#,a'length)); 
        cin <= '0'; 

        -- Set expected output values
        expected_sum <= std_logic_vector(to_unsigned(16#0B#,a'length)); 
        expected_cout <= '0'; 
        expected_p <= '0'; 
        expected_g <= '0'; 

        wait for 20 ns; 

        -- Assert on sum 
        assert sum = expected_sum
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Sum error." & LF & "Expected value = " & 
            integer'image(to_integer(unsigned(expected_sum))) & LF &
            "got value = " & integer'image(to_integer(unsigned(sum)))
            severity error;

        -- Assert on carry out
        assert cout = expected_cout
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Carry out error." & LF & "Expected value = " & 
            std_logic'image(expected_cout) & LF &
            "got value = " & std_logic'image(cout)
            severity error;

        -- Assert on p out
        assert p = expected_p
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": P out error." & LF & "Expected value = " & 
            std_logic'image(expected_p) & LF &
            "got value = " & std_logic'image(p)
            severity error;

        -- Assert on g out
        assert g = expected_g
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": G out error." & LF & "Expected value = " & 
            std_logic'image(expected_g) & LF &
            "got value = " & std_logic'image(g)
            severity error;
        case_no := case_no + 1; 
        
        -- Test case 3 ########################################################

        -- Set input values
        a <= std_logic_vector(to_unsigned(16#1A#,a'length)); 
        b <= std_logic_vector(to_unsigned(16#08#,a'length)); 
        cin <= '0'; 

        -- Set expected output values
        expected_sum <= std_logic_vector(to_unsigned(16#22#,a'length)); 
        expected_cout <= '0'; 
        expected_p <= '0'; 
        expected_g <= '0'; 

        wait for 20 ns; 

        -- Assert on sum 
        assert sum = expected_sum
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Sum error." & LF & "Expected value = " & 
            integer'image(to_integer(unsigned(expected_sum))) & LF &
            "got value = " & integer'image(to_integer(unsigned(sum)))
            severity error;

        -- Assert on carry out
        assert cout = expected_cout
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Carry out error." & LF & "Expected value = " & 
            std_logic'image(expected_cout) & LF &
            "got value = " & std_logic'image(cout)
            severity error;

        -- Assert on p out
        assert p = expected_p
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": P out error." & LF & "Expected value = " & 
            std_logic'image(expected_p) & LF &
            "got value = " & std_logic'image(p)
            severity error;

        -- Assert on g out
        assert g = expected_g
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": G out error." & LF & "Expected value = " & 
            std_logic'image(expected_g) & LF &
            "got value = " & std_logic'image(g)
            severity error;
        case_no := case_no + 1; 

        -- Test case 4 ########################################################

        -- Set input values
        a <= std_logic_vector(to_unsigned(16#1A#,a'length)); 
        b <= std_logic_vector(to_unsigned(16#11#,a'length)); 
        cin <= '1'; 

        -- Set expected output values
        expected_sum <= std_logic_vector(to_unsigned(16#2C#,a'length)); 
        expected_cout <= '0'; 
        expected_p <= '0'; 
        expected_g <= '0'; 

        wait for 20 ns; 

        -- Assert on sum 
        assert sum = expected_sum
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Sum error." & LF & "Expected value = " & 
            integer'image(to_integer(unsigned(expected_sum))) & LF &
            "got value = " & integer'image(to_integer(unsigned(sum)))
            severity error;

        -- Assert on carry out
        assert cout = expected_cout
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Carry out error." & LF & "Expected value = " & 
            std_logic'image(expected_cout) & LF &
            "got value = " & std_logic'image(cout)
            severity error;

        -- Assert on p out
        assert p = expected_p
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": P out error." & LF & "Expected value = " & 
            std_logic'image(expected_p) & LF &
            "got value = " & std_logic'image(p)
            severity error;

        -- Assert on g out
        assert g = expected_g
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": G out error." & LF & "Expected value = " & 
            std_logic'image(expected_g) & LF &
            "got value = " & std_logic'image(g)
            severity error;
        case_no := case_no + 1; 


        -- Test case 5 ########################################################

        -- Set input values
        a <= std_logic_vector(to_unsigned(16#D2A#,a'length)); 
        b <= std_logic_vector(to_unsigned(16#D4#,a'length)); 
        cin <= '1'; 

        -- Set expected output values
        expected_sum <= std_logic_vector(to_unsigned(16#DFF#,a'length)); 
        expected_cout <= '0'; 
        expected_p <= '0'; 
        expected_g <= '0'; 

        wait for 20 ns; 

        -- Assert on sum 
        assert sum = expected_sum
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Sum error." & LF & "Expected value = " & 
            integer'image(to_integer(unsigned(expected_sum))) & LF &
            "got value = " & integer'image(to_integer(unsigned(sum)))
            severity error;

        -- Assert on carry out
        assert cout = expected_cout
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Carry out error." & LF & "Expected value = " & 
            std_logic'image(expected_cout) & LF &
            "got value = " & std_logic'image(cout)
            severity error;

        -- Assert on p out
        assert p = expected_p
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": P out error." & LF & "Expected value = " & 
            std_logic'image(expected_p) & LF &
            "got value = " & std_logic'image(p)
            severity error;

        -- Assert on g out
        assert g = expected_g
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": G out error." & LF & "Expected value = " & 
            std_logic'image(expected_g) & LF &
            "got value = " & std_logic'image(g)
            severity error;
        case_no := case_no + 1; 

        -- Test case 6 ########################################################

        -- Set input values
        a <= std_logic_vector(to_unsigned(16#D28#,a'length)); 
        b <= std_logic_vector(to_unsigned(16#D4#,a'length)); 
        cin <= '0'; 

        -- Set expected output values
        expected_sum <= std_logic_vector(to_unsigned(16#DFC#,a'length)); 
        expected_cout <= '0'; 
        expected_p <= '0'; 
        expected_g <= '0'; 

        wait for 20 ns; 

        -- Assert on sum 
        assert sum = expected_sum
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Sum error." & LF & "Expected value = " & 
            integer'image(to_integer(unsigned(expected_sum))) & LF &
            "got value = " & integer'image(to_integer(unsigned(sum)))
            severity error;

        -- Assert on carry out
        assert cout = expected_cout
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Carry out error." & LF & "Expected value = " & 
            std_logic'image(expected_cout) & LF &
            "got value = " & std_logic'image(cout)
            severity error;

        -- Assert on p out
        assert p = expected_p
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": P out error." & LF & "Expected value = " & 
            std_logic'image(expected_p) & LF &
            "got value = " & std_logic'image(p)
            severity error;

        -- Assert on g out
        assert g = expected_g
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": G out error." & LF & "Expected value = " & 
            std_logic'image(expected_g) & LF &
            "got value = " & std_logic'image(g)
            severity error;
        case_no := case_no + 1;  

        -- Test case 7 ########################################################

        -- Set input values
        a <= std_logic_vector(to_unsigned(16#6D3A#,a'length)); 
        b <= std_logic_vector(to_unsigned(16#300#,a'length)); 
        cin <= '0'; 

        -- Set expected output values
        expected_sum <= std_logic_vector(to_unsigned(16#703A#,a'length)); 
        expected_cout <= '0'; 
        expected_p <= '0'; 
        expected_g <= '0'; 

        wait for 20 ns; 

        -- Assert on sum 
        assert sum = expected_sum
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Sum error." & LF & "Expected value = " & 
            integer'image(to_integer(unsigned(expected_sum))) & LF &
            "got value = " & integer'image(to_integer(unsigned(sum)))
            severity error;

        -- Assert on carry out
        assert cout = expected_cout
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Carry out error." & LF & "Expected value = " & 
            std_logic'image(expected_cout) & LF &
            "got value = " & std_logic'image(cout)
            severity error;

        -- Assert on p out
        assert p = expected_p
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": P out error." & LF & "Expected value = " & 
            std_logic'image(expected_p) & LF &
            "got value = " & std_logic'image(p)
            severity error;

        -- Assert on g out
        assert g = expected_g
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": G out error." & LF & "Expected value = " & 
            std_logic'image(expected_g) & LF &
            "got value = " & std_logic'image(g)
            severity error;
        case_no := case_no + 1; 

        -- Test case 8 ########################################################

        -- Set input values
        a <= std_logic_vector(to_unsigned(16#6D3A#,a'length)); 
        b <= std_logic_vector(to_unsigned(16#300#,a'length)); 
        cin <= '0'; 

        -- Set expected output values
        expected_sum <= std_logic_vector(to_unsigned(16#703A#,a'length)); 
        expected_cout <= '0'; 
        expected_p <= '0'; 
        expected_g <= '0'; 

        wait for 20 ns; 

        -- Assert on sum 
        assert sum = expected_sum
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Sum error." & LF & "Expected value = " & 
            integer'image(to_integer(unsigned(expected_sum))) & LF &
            "got value = " & integer'image(to_integer(unsigned(sum)))
            severity error;

        -- Assert on carry out
        assert cout = expected_cout
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Carry out error." & LF & "Expected value = " & 
            std_logic'image(expected_cout) & LF &
            "got value = " & std_logic'image(cout)
            severity error;

        -- Assert on p out
        assert p = expected_p
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": P out error." & LF & "Expected value = " & 
            std_logic'image(expected_p) & LF &
            "got value = " & std_logic'image(p)
            severity error;

        -- Assert on g out
        assert g = expected_g
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": G out error." & LF & "Expected value = " & 
            std_logic'image(expected_g) & LF &
            "got value = " & std_logic'image(g)
            severity error;
        case_no := case_no + 1; 

        -- Test case 9 ########################################################

        -- Set input values
        a <= std_logic_vector(to_unsigned(16#6D3A#,a'length)); 
        b <= std_logic_vector(to_unsigned(16#300#,a'length)); 
        cin <= '1'; 

        -- Set expected output values
        expected_sum <= std_logic_vector(to_unsigned(16#703B#,a'length)); 
        expected_cout <= '0'; 
        expected_p <= '0'; 
        expected_g <= '0'; 

        wait for 20 ns; 

        -- Assert on sum 
        assert sum = expected_sum
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Sum error." & LF & "Expected value = " & 
            integer'image(to_integer(unsigned(expected_sum))) & LF &
            "got value = " & integer'image(to_integer(unsigned(sum)))
            severity error;

        -- Assert on carry out
        assert cout = expected_cout
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Carry out error." & LF & "Expected value = " & 
            std_logic'image(expected_cout) & LF &
            "got value = " & std_logic'image(cout)
            severity error;

        -- Assert on p out
        assert p = expected_p
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": P out error." & LF & "Expected value = " & 
            std_logic'image(expected_p) & LF &
            "got value = " & std_logic'image(p)
            severity error;

        -- Assert on g out
        assert g = expected_g
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": G out error." & LF & "Expected value = " & 
            std_logic'image(expected_g) & LF &
            "got value = " & std_logic'image(g)
            severity error;
        case_no := case_no + 1; 

        -- Test case 10 ########################################################

        -- Set input values
        a <= std_logic_vector(to_unsigned(16#9A36#,a'length)); 
        b <= std_logic_vector(to_unsigned(16#7D3D#,a'length)); 
        cin <= '0'; 

        -- Set expected output values
        expected_sum <= std_logic_vector(to_unsigned(16#1773#,a'length)); 
        expected_cout <= '1'; 
        expected_p <= '0'; 
        expected_g <= '0'; 

        wait for 20 ns; 

        -- Assert on sum 
        assert sum = expected_sum
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Sum error." & LF & "Expected value = " & 
            integer'image(to_integer(unsigned(expected_sum))) & LF &
            "got value = " & integer'image(to_integer(unsigned(sum)))
            severity error;

        -- Assert on carry out
        assert cout = expected_cout
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": Carry out error." & LF & "Expected value = " & 
            std_logic'image(expected_cout) & LF &
            "got value = " & std_logic'image(cout)
            severity error;

        -- Assert on p out
        assert p = expected_p
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": P out error." & LF & "Expected value = " & 
            std_logic'image(expected_p) & LF &
            "got value = " & std_logic'image(p)
            severity error;

        -- Assert on g out
        assert g = expected_g
            report "" & LF & "Test Case " & integer'image(case_no) & 
            ": G out error." & LF & "Expected value = " & 
            std_logic'image(expected_g) & LF &
            "got value = " & std_logic'image(g)
            severity error;
        case_no := case_no + 1; 

        assert false report "end of test" severity note; 

        wait; 
        end process; 
end behav; 