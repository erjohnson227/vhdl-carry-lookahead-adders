library ieee; 
use ieee.std_logic_1164.all; 

entity full_adder_tb is 
end full_adder_tb; 

architecture behav of full_adder_tb is 

    component full_adder
        port (i_ina, i_inb, i_cin : in std_logic; o_sum, o_carry : out std_logic); 
    end component; 

    for full_adder_0: full_adder use entity work.full_adder; 

    signal ia, ib, cin, sum, carry : std_logic; 

    begin
        full_adder_0: full_adder port map(i_ina => ia, i_inb => ib, 
                                          i_cin => cin, o_sum => sum, 
                                          o_carry => carry);

    process
        type pattern_type is record
            ia, ib, cin     : std_logic; 
            sum, carry      : std_logic; 
        end record; 

        type pattern_array is array (natural range <>) of pattern_type; 
        constant patterns : pattern_array := 
        (('0', '0', '0', '0', '0'),
         ('1', '0', '0', '1', '0'),
         ('0', '1', '0', '1', '0'),
         ('1', '1', '0', '0', '1'),
         ('0', '0', '1', '1', '0'),
         ('1', '0', '1', '0', '1'),
         ('0', '1', '1', '0', '1'),
         ('1', '1', '1', '1', '1')); 

        begin
            for i in patterns'range loop 
                    ia <= patterns(i).ia; 
                    ib <= patterns(i).ib; 
                    cin <= patterns(i).cin; 

                    wait for 1 ns; 

                    assert sum = patterns(i).sum
                        report "Bad sum value" severity error; 
                    
                    assert carry = patterns(i).carry
                        report "Bad carry value" severity error; 
                end loop;

                assert false report "end of test" severity note; 
                wait; 
        end process; 
end behav; 