library ieee; 
use ieee.std_logic_1164.all; 

entity lookahead_adder_4bit_tb is
end lookahead_adder_4bit_tb; 

architecture behav of lookahead_adder_4bit_tb is 

component lookahead_adder_4bit
    port (
        i4_a : in std_logic_vector(3 downto 0); 
        i4_b : in std_logic_vector(3 downto 0); 
        i_cin : in std_logic; 
        o_sum : out std_logic_vector(3 downto 0); 
        o_cout : out std_logic;
        o_p : out std_logic;
        o_g : out std_logic 
    );
end component; 

for lookahead_adder_4bit_0 : lookahead_adder_4bit use entity
                                     work.lookahead_adder_4bit; 

-- Signal definitions
signal a, b : std_logic_vector(3 downto 0);
signal cin : std_logic;  
signal sum : std_logic_vector(3 downto 0); 
signal cout : std_logic; 
signal p, g : std_logic; 

begin 
    lookahead_adder_4bit_0: lookahead_adder_4bit port map(
        i4_a => a, i4_b => b, i_cin => cin, o_sum => sum, o_cout => cout, o_p => p, o_g => g
    );

    process
    begin 

    

    a <= "0000"; 
    b <= "0000"; 
    cin <= '0'; 

    wait for 20 ns; 

    assert sum = "0000"
        report "Sum error" severity error; 

    assert cout = '0'
        report "Carry error" severity error; 

    a <= "0001"; 
    b <= "0000"; 
    cin <= '0'; 

    wait for 20 ns; 

    assert sum = "0001"
        report "Sum error" severity error; 

    assert cout = '0'
        report "Carry error" severity error; 

    a <= "0001"; 
    b <= "0001"; 
    cin <= '0'; 

    wait for 20 ns;

    assert sum = "0010"
        report "Sum error" severity error; 

    assert cout = '0'
        report "Carry error" severity error; 

    a <= "0010"; 
    b <= "0000"; 
    cin <= '0'; 

    wait for 20 ns; 

    assert sum = "0010"
        report "Sum error" severity error; 

    assert cout = '0'
        report "Carry error" severity error; 
    
    a <= "0001"; 
    b <= "0000"; 
    cin <= '1'; 

    wait for 20 ns; 

    assert sum = "0010"
        report "Sum error" severity error; 

    assert cout = '0'
        report "Carry error" severity error; 

    a <= "0001"; 
    b <= "0001"; 
    cin <= '1'; 

    wait for 20 ns; 

    assert sum = "0011"
        report "Sum error" severity error; 

    assert cout = '0'
        report "Carry error" severity error; 
    
    a <= "0011"; 
    b <= "0001"; 
    cin <= '0'; 

    wait for 20 ns; 

    assert sum = "0100"
        report "Sum error" severity error; 

    assert cout = '0'
        report "Carry error" severity error;  
    
    a <= "0001"; 
    b <= "0011"; 
    cin <= '1'; 

    wait for 20 ns; 

    assert sum = "0101"
        report "Sum error" severity error; 

    assert cout = '0'
        report "Carry error" severity error; 
    
    a <= "0101"; 
    b <= "0011"; 
    cin <= '0'; 

    wait for 20 ns; 

    assert sum = "1000"
        report "Sum error" severity error; 

    assert cout = '0'
        report "Carry error" severity error; 
    
    a <= "0101"; 
    b <= "0011"; 
    cin <= '1'; 

    wait for 20 ns; 

    assert sum = "1001"
        report "Sum error" severity error; 

    assert cout = '0'
        report "Carry error" severity error;  
    
    a <= "1011"; 
    b <= "1101"; 
    cin <= '0'; 

    wait for 20 ns; 

    assert sum = "1000"
        report "Sum error" severity error; 

    assert cout = '1'
        report "Carry error" severity error; 

    assert false report "end of test" severity note; 
    wait; 
    end process;

end behav;