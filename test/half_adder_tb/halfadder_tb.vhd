library ieee; 
use ieee.std_logic_1164.all; 

entity half_adder_tb is 
end half_adder_tb; 

architecture behav of half_adder_tb is 
    component half_adder
        port (i_b0, i_b1 :in std_logic; o_s : out std_logic; o_c : out std_logic); 
    end component; 

    for half_adder_0 : half_adder use entity work.half_adder;

    signal i0, i1, s, c : std_logic; 

    begin
        half_adder_0: half_adder port map (i_b0 => i0, i_b1 => i1, o_s => s, o_c => c); 

    process
        type pattern_type is record
            i0, i1  : std_logic; 
            s, c    : std_logic; 
        end record; 

        type pattern_array is array (natural range <>) of pattern_type; 
        constant patterns : pattern_array := 
            (('0', '0', '0', '0'),
            ('1', '0', '1', '0'),
            ('0', '1', '1', '0'),
            ('1', '1', '0', '1'));
        
        begin 
            for i in patterns'range loop
                i0 <= patterns(i).i0; 
                i1 <= patterns(i).i1; 

                wait for 1 ns; 

                assert s = patterns(i).s
                    report "bad sum value" severity error; 
                assert c = patterns(i).c
                    report "bad carry value" severity error; 

            end loop;

            assert false report "end of test" severity note;

            wait; 
            
    end process; 
end behav;