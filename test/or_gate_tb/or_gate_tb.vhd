library ieee; 
use ieee.std_logic_1164.all; 

entity or_gate_tb is 
end or_gate_tb; 

architecture behav of or_gate_tb is 
     
    component or_gate
        port(i_a, i_b : in std_logic; o_out : out std_logic);
    end component; 

    for or_gate_0 : or_gate use entity work.or_gate;

    signal ia, ib, o : std_logic;

    begin
        or_gate_0: or_gate port map (i_a => ia, i_b => ib, o_out => o); 

    process
        type pattern_type is record
            ia, ib  : std_logic; 
            o       : std_logic; 
        end record; 

        type pattern_array is array (natural range <>) of pattern_type; 
        constant patterns : pattern_array := 
            (('0', '0', '0'),
             ('1', '0', '1'),
             ('0', '1', '1'),
             ('1', '1', '1'));
        
        begin 
            for i in patterns'range loop
                ia <= patterns(i).ia; 
                ib <= patterns(i).ib; 

                wait for 1 ns; 

                assert o = patterns(i).o
                    report "bad output value" severity error; 
            end loop;

            assert false report "end of test" severity note;

            wait; 
            
    end process; 
end behav;