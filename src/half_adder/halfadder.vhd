library ieee; 
use ieee.std_logic_1164.all; 


entity half_adder is
    port(
        i_b0  : in std_logic; 
        i_b1  : in std_logic; 
        o_s   : out std_logic; 
        o_c : out std_logic
    );
end half_adder;

architecture rtl of half_adder is
begin
    o_s <= i_b0 xor i_b1; 
    o_c <= i_b0 and i_b1; 
end rtl; 

