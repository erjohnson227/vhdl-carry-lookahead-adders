library ieee; 
use ieee.std_logic_1164.all; 

entity or_gate is 
    port(
        i_a : in std_logic; 
        i_b : in std_logic; 
        o_out : out std_logic
    ); 
end or_gate; 

architecture rtl of or_gate is 
begin
    o_out <= i_a or i_b; 
end rtl; 