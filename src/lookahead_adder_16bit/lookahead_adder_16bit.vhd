library ieee; 
use ieee.std_logic_1164.all; 

entity lookahead_adder_16bit is
port (
    i16_a : in std_logic_vector(15 downto 0); 
    i16_b : in std_logic_vector(15 downto 0); 
    i_cin : in std_logic; 
    o16_sum : out std_logic_vector(15 downto 0); 
    o_cout : out std_logic;
    o_p : out std_logic; 
    o_g : out std_logic
);
end lookahead_adder_16bit; 

architecture structural of lookahead_adder_16bit is 

    signal carry1, carry2, carry3 : std_logic; 
    signal p0, p1, p2, p3 : std_logic; 
    signal g0, g1, g2, g3 : std_logic;
    signal p, g : std_logic_vector(3 downto 0); 
    signal c : std_logic_vector(3 downto 0);

    component lookahead_4bit
        port (i_cin : in std_logic;
                i_p : in std_logic_vector(3 downto 0);
                i_g : in std_logic_vector(3 downto 0); 
                o_c : out std_logic_vector(3 downto 0);
                o_p : out std_logic; 
                o_g : out std_logic
                ); 
    end component; 

    component lookahead_adder_4bit is
        port (i4_a : in std_logic_vector(3 downto 0); 
             i4_b : in std_logic_vector(3 downto 0); 
             i_cin : in std_logic; 
             o_sum : out std_logic_vector(3 downto 0); 
             o_cout : out std_logic;
             o_p : out std_logic; 
             o_g : out std_logic
        );
    end component; 

    for lookahead_adder_4bit_0: lookahead_adder_4bit 
                            use entity work.lookahead_adder_4bit; 
    for lookahead_adder_4bit_1: lookahead_adder_4bit 
                            use entity work.lookahead_adder_4bit;
    for lookahead_adder_4bit_2: lookahead_adder_4bit 
                            use entity work.lookahead_adder_4bit; 
    for lookahead_adder_4bit_3: lookahead_adder_4bit 
                            use entity work.lookahead_adder_4bit; 

    for lookahead_4bit_0 : lookahead_4bit use entity work.lookahead_4bit; 

    begin 

        p <= p3 & p2 & p1 & p0; 
        g <= g3 & g2 & g1 & g0; 

        carry1 <= c(0); 
        carry2 <= c(1); 
        carry3 <= c(2); 
        o_cout <= c(3); 

        -- Set up connections for 4bit carry lookahead adder 1
        lookahead_adder_4bit_0 : lookahead_adder_4bit port map(
            i_cin => i_cin,
            i4_a => i16_a(3 downto 0),
            i4_b => i16_b(3 downto 0),
            o_sum => o16_sum(3 downto 0),
            o_cout => open, 
            o_p => p0, 
            o_g => g0
        );

        -- Set up connections for 4bit carry lookahead adder 2
        lookahead_adder_4bit_1 : lookahead_adder_4bit port map(
            i_cin => carry1,
            i4_a => i16_a(7 downto 4),
            i4_b => i16_b(7 downto 4),
            o_sum => o16_sum(7 downto 4),
            o_cout => open, 
            o_p => p1, 
            o_g => g1
        );

        -- Set up connections for 4bit carry lookahead adder 3
        lookahead_adder_4bit_2 : lookahead_adder_4bit port map(
            i_cin => carry2,
            i4_a => i16_a(11 downto 8),
            i4_b => i16_b(11 downto 8),
            o_sum => o16_sum(11 downto 8),
            o_cout => open, 
            o_p => p2, 
            o_g => g2
        );

        -- Set up connections for 4bit carry lookahead adder 4
        lookahead_adder_4bit_3 : lookahead_adder_4bit port map(
            i_cin => carry3,
            i4_a => i16_a(15 downto 12),
            i4_b => i16_b(15 downto 12),
            o_sum => o16_sum(15 downto 12),
            o_cout => open, 
            o_p => p3, 
            o_g => g3
        );

        lookahead_4bit_0 : lookahead_4bit port map(
            i_cin => i_cin,
            i_p => p, 
            i_g => g, 
            o_c => c,
            o_p => o_p, 
            o_g => o_g
        ); 

end structural; 