library ieee; 
use ieee.std_logic_1164.all; 

entity lookahead_adder_4bit is
port (
    i4_a : in std_logic_vector(3 downto 0); 
    i4_b : in std_logic_vector(3 downto 0); 
    i_cin : in std_logic; 
    o_sum : out std_logic_vector(3 downto 0); 
    o_cout : out std_logic;
    o_p : out std_logic; 
    o_g : out std_logic
);
end lookahead_adder_4bit; 

architecture structural of lookahead_adder_4bit is 

    signal carry1, carry2, carry3 : std_logic; 
    signal p0, p1, p2, p3 : std_logic; 
    signal g0, g1, g2, g3 : std_logic; 
    signal p, g : std_logic_vector(3 downto 0); 
    signal c : std_logic_vector(3 downto 0); 

    component full_adder_pg
        port (i_a, i_b, i_cin : in std_logic; 
              o_p, o_g, o_sum : out std_logic
              ); 
    end component; 

    component lookahead_4bit
        port (i_cin : in std_logic;
              i_p : in std_logic_vector(3 downto 0);
              i_g : in std_logic_vector(3 downto 0); 
              o_c : out std_logic_vector(3 downto 0);
              o_p : out std_logic; 
              o_g : out std_logic
              ); 
    end component; 

    for full_adder_0 : full_adder_pg use entity work.full_adder_pg; 
    for full_adder_1 : full_adder_pg use entity work.full_adder_pg; 
    for full_adder_2 : full_adder_pg use entity work.full_adder_pg; 
    for full_adder_3 : full_adder_pg use entity work.full_adder_pg; 
    for lookahead_4bit_0 : lookahead_4bit use entity work.lookahead_4bit; 

    begin

        p <= p3 & p2 & p1 & p0; 
        g <= g3 & g2 & g1 & g0; 

        --o_cout & carry3 & carry2 & carry1  <= c;

        carry1 <= c(0); 
        carry2 <= c(1); 
        carry3 <= c(2); 
        o_cout <= c(3);  

        full_adder_0 : full_adder_pg port map(i_a => i4_a(0),
                                           i_b => i4_b(0),
                                           i_cin => i_cin,
                                           o_p => p0, 
                                           o_g => g0, 
                                           o_sum => o_sum(0)); 

        full_adder_1 : full_adder_pg port map(i_a => i4_a(1),
                                           i_b => i4_b(1),
                                           i_cin => carry1,
                                           o_p => p1, 
                                           o_g => g1, 
                                           o_sum => o_sum(1)); 

        full_adder_2 : full_adder_pg port map(i_a => i4_a(2),
                                           i_b => i4_b(2),
                                           i_cin => carry2,
                                           o_p => p2, 
                                           o_g => g2, 
                                           o_sum => o_sum(2)); 

        full_adder_3 : full_adder_pg port map(i_a => i4_a(3),
                                           i_b => i4_b(3),
                                           i_cin => carry3,
                                           o_p => p3, 
                                           o_g => g3, 
                                           o_sum => o_sum(3));

        lookahead_4bit_0 : lookahead_4bit port map(i_cin => i_cin,
                                             i_p => p, 
                                             i_g => g, 
                                             o_c => c,
                                             o_p => o_p, 
                                             o_g => o_g); 
                                           
end structural; 