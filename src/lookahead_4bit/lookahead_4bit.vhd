library ieee; 
use ieee.std_logic_1164.all; 

entity lookahead_4bit is
    port(
        i_cin : in std_logic; 
        i_p : in std_logic_vector(3 downto 0); 
        i_g : in std_logic_vector(3 downto 0); 
        o_c : out std_logic_vector(3 downto 0);
        o_p : out std_logic; 
        o_g : out std_logic
    ); 
end lookahead_4bit; 

architecture rtl of lookahead_4bit is
begin 
    -- Carry lookahead signals
    o_c(0) <= i_g(0) or (i_p(0) and i_cin);
    o_c(1) <= (i_g(1) or (i_p(1) and i_g(0)) or (i_p(1) and i_p(0) and i_cin)); 
    o_c(2) <= i_g(2) or (i_p(2) and i_g(1)) or (i_p(2) and i_p(1) and i_g(0)) or
              (i_p(2) and i_p(1) and i_p(0) and i_cin);
    o_c(3) <= i_g(3) or (i_p(3) and i_g(2)) or (i_p(3) and i_p(2) and i_g(1)) or 
              (i_p(3) and i_p(2) and i_p(1) and i_g(0)) or
              (i_p(3) and i_p(2) and i_p(1) and i_p(0) and i_cin); 

    -- Super prpopagate and super generate signals out
    o_p <= i_p(0) and i_p(1) and i_p(2) and i_p(3); 
    o_g <= i_g(3) or (i_p(3) and i_g(2)) or (i_p(3) and i_p(2) and i_g(1)) or
           (i_p(3) and i_p(2) and i_p(1) and i_g(0));   
end rtl; 

