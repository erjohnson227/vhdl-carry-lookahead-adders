library ieee; 
use ieee.std_logic_1164.all; 

entity full_adder_pg is 
    port(
        i_a     : in std_logic; 
        i_b     : in std_logic; 
        i_cin   : in std_logic; 
        o_p     : out std_logic; 
        o_g     : out std_logic; 
        o_sum   : out std_logic
    );
end full_adder_pg; 

architecture rtl of full_adder_pg is

begin 
    -- Output signals for propagate, generate, and sum. 
    o_p <= i_a xor i_b; 
    o_g <= i_a and i_b;
    o_sum <= i_a xor i_b xor i_cin; 
    
end rtl; 
