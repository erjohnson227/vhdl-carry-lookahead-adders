library ieee; 
use ieee.std_logic_1164.all; 

entity full_adder is
    port (
        i_ina   : in std_logic; 
        i_inb   : in std_logic; 
        i_cin   : in std_logic; 
        o_sum   : out std_logic; 
        o_carry : out std_logic
    ); 
end full_adder; 

architecture structural of full_adder is

signal sum1, carry1, carry2: std_logic;

component half_adder
    port (i_b0, i_b1 : in std_logic; o_s, o_c : out std_logic);
end component; 

component or_gate
    port (i_a, i_b : in std_logic; o_out : out std_logic);
end component; 

for half_adder_0 : half_adder use entity work.half_adder; 
for half_adder_1 : half_adder use entity work.half_adder; 
for or_gate_0 : or_gate use entity work.or_gate; 
begin
    half_adder_0 : half_adder port map (i_b0 => i_ina, i_b1 => i_inb, 
                                        o_s => sum1, o_c => carry1); 

    half_adder_1 : half_adder port map (i_b0 => i_cin, i_b1 => sum1, 
                                        o_s => o_sum, o_c => carry2);

    or_gate_0 : or_gate port map (i_a => carry2, i_b => carry1, 
                                  o_out => o_carry); 
end structural; 
